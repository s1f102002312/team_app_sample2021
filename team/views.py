from django.shortcuts import render,redirect
from django.http import Http404, JsonResponse
from django.core.exceptions import SuspiciousFileOperation
from django.views.decorators.csrf import csrf_exempt
import urllib
import json
import random

from .models import Reply

WEBHOOK_URL = 'https://hooks.slack.com/services/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'
VERIFICATION_TOKEN = '1mnfC2tyD1ANQ8Y0m6atAs7g'
ACTION_HOW_ARE_YOU = 'HOW_ARE_YOU'

def index(request):
    positive_replies = Reply.objects.filter(response=Reply.POSITIVE)
    neutral_replies = Reply.objects.filter(response=Reply.NEUTRAL)
    negative_replies = Reply.objects.filter(response=Reply.NEGATIVE)

    context = {
        'positive_replies': positive_replies,
        'neutral_replies': neutral_replies,
        'negative_replies': negative_replies,

    }
    return render(request, 'index.html', context)

def clear(request):
    Reply.objects.all().delete()
    return redirect(index)

def announce(request):
    if request.method == 'POST':
        data = {
            'text': request.POST['message']
        }
        post_message(WEBHOOK_URL, data)

    return redirect(index)

@csrf_exempt
def echo(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    
    user_name = request.POST['user_name']
    user_id = request.POST['user_id']
    content = request.POST['text']

    result = {
        'text': '<@{}> {}'.format(user_id, content.upper()),
        'response_type': 'in_channel'
    }

    return JsonResponse(result)

@csrf_exempt
def hello(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    
    user_name = request.POST['user_name']
    user_id = request.POST['user_id']
    content = request.POST['text']

    result = {
        'blocks': [
            {
                'type' : 'section',
                'text' : {
                    'type': 'mrkdwn',
                    'text': '<@{}> どのコンビニがいいですか?'.format(user_id)
                },
                'accessory': {
                    'type': 'static_select',
                    'placeholder': {
                        'type': 'plain_text',
                        'text': '私は…',
                        'emoji': True
                    },
                    'options': [
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'ファミリーマート',
                                'emoji': True
                            },
                            'value': 'family mart'
                        },
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'セブンイレブン',
                                'emoji': True
                            },
                            'value': 'seven eleven'
                        },
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'ローソン',
                                'emoji': True
                            },
                            'value': 'LAWSON'
                        }
                    ],
                    'action_id': ACTION_HOW_ARE_YOU
                }
            }
        ],
        'response_type': 'in_channel'
    }

    return JsonResponse(result)

@csrf_exempt
def sake(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    
    user_name = request.POST['user_name']
    user_id = request.POST['user_id']
    content = request.POST['text']

    result = {
        'blocks': [
            {
                'type' : 'section',
                'text' : {
                    'type': 'mrkdwn',
                    'text': '<@{}> どのコンビニがいいですか?'.format(user_id)
                },
                'accessory': {
                    'type': 'static_select',
                    'placeholder': {
                        'type': 'plain_text',
                        'text': '私は…',
                        'emoji': True
                    },
                    'options': [
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'ファミリーマート',
                                'emoji': True
                            },
                            'value': 'family mart'
                        },
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'セブンイレブン',
                                'emoji': True
                            },
                            'value': 'seven eleven'
                        },
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'ローソン',
                                'emoji': True
                            },
                            'value': 'LAWSON'
                        }
                    ],
                    'action_id': ACTION_HOW_ARE_YOU
                }
            }
        ],
        'response_type': 'in_channel'
    }

    return JsonResponse(result)



@csrf_exempt
def reply(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    payload = json.loads(request.POST.get('payload'))
    print(payload)
    if payload.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    
    if payload['actions'][0]['action_id'] != ACTION_HOW_ARE_YOU:
        raise SuspiciousOperation('Invalid request.')
    
    user = payload['user']
    selected_value = payload['actions'][0]['selected_option']['value']
    response_url = payload['response_url']

    if selected_value == 'family mart':
        reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.POSITIVE)
        reply.save()
        ls = ["おむすびはセブンにも負けません！""https://www.family.co.jp/goods/omusubi.html","パンは新商品続々登場！""https://www.family.co.jp/goods/bread.html","コーヒーのうまさはコンビニ1番！""https://www.family.co.jp/goods/cafe.html","定番、ファミチキはいかがでしょう？""https://www.family.co.jp/goods/friedfoods.html","弁当は種類が豊富！""https://www.family.co.jp/goods/obento.html"]
        n = random.randint(0,5)
        response = {
           'text': '<@{0}> ファミリーマートの {1} '.format(user['id'],ls[n])
        }
    elif selected_value == 'seven eleven':
        reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.NEUTRAL)
        reply.save()
        ls = ["熱々おでんはどうでしょう""https://www.sej.co.jp/products/a/oden/","おにぎりも日本一！！""https://www.sej.co.jp/products/a/onigiri/","サンドイッチしか勝たん""https://www.sej.co.jp/products/a/sandwich/","夏こそ熱々中華まん！！""https://www.sej.co.jp/products/a/chukaman/","日本人ならパスタだよ""https://www.sej.co.jp/products/a/pasta/"]
        n = random.randint(0,len(ls))
        response = {
            'text': '<@{0}> セブンね( ˘ω˘) セブンの食べ物は日本一です  {1}'.format(user['id'],ls[n])
        }
    else:
        reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.NEGATIVE)
        reply.save()

        ls = ["おにぎりは定番""https://www.lawson.co.jp/recommend/original/rice/","サンドウィッチもおすすめ！""https://www.lawson.co.jp/recommend/original/sandwich/","ベーカリーはどう？""https://www.lawson.co.jp/recommend/original/bakery/","パスタはセブン超え！？""https://www.lawson.co.jp/recommend/original/pasta/","グラタンもおいしいよ""https://www.lawson.co.jp/recommend/original/gratin/"]
        n = random.randint(0,len(ls))

        response = {
            'text': '<@{0}> ローソンの {1} '.format(user['id'],ls[n])
        }
    
    post_message(response_url, response)

    return JsonResponse({})

def post_message(url, data):
    headers = {
        'Content-Type': 'application/json',
    }
    req = urllib.request.Request(url, json.dumps(data).encode(), headers)
    with urllib.request.urlopen(req) as res:
        body = res.read()


